Grails 2 transactional rollback demo
====================================

Scaffold project for testing [this StackOverflow answer](https://stackoverflow.com/a/47460653)
to a question about rolling back transactional service methods.

## Usage

    grails test-app

