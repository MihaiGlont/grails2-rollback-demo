package rollback

import grails.transaction.Transactional
import groovy.transform.CompileStatic

/**
 * Simple demo showing how to issue a rollback from a transactional service method.
 * Test for <a href='https://stackoverflow.com/questions/28242153/'>this StackOverflow answer</a>
 */
@CompileStatic
class FooService {

    /**
     * Grails turns the save() method below into something like
     * <pre>
     * def save() {
     *     new TransactionCallback( { status ->
     *         fooService.save(value)
     *      }
     *     ).execute()
     * }
     * </pre>
     *
     * <p>The transactionStatus variable is implicitly defined and made available
     * thanks to the AST transformation applied to <b>transactional methods only</b>
     * during the compilation process.
     * @see org.codehaus.groovy.grails.transaction.transform.TransactionalTransform
     */
    @Transactional
    def save(String value) {
        def entity = new Foo(value: value)

        if (!entity.save()) {
            transactionStatus.setRollbackOnly()
        }

        [entity: entity, tx: transactionStatus]
    }
}
