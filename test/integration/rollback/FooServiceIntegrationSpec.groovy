package rollback

import grails.test.spock.IntegrationSpec

class FooServiceIntegrationSpec extends IntegrationSpec {

    FooService fooService

    void "test rolling back after saving an invalid entity"() {
        when:
        def entity = fooService.save((String) null)?.entity

        then:
        noExceptionThrown()
        !entity.id
        entity.errors.allErrors.size() == 1
    }
}
