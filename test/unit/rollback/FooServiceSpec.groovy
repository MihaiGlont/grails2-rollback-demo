package rollback

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.springframework.transaction.TransactionStatus
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(FooService)
@Mock(Foo)
class FooServiceSpec extends Specification {

    void "test rolling back after saving an invalid entity"() {
        when:
        def result = service.save((String) null)
        def entity = result.entity
        def status = result.tx as TransactionStatus

        then:
        noExceptionThrown()

        !entity.id
        entity.errors.allErrors.size() == 1
        0 == Foo.count()

        status.rollbackOnly
        status.isCompleted()

    }
}
