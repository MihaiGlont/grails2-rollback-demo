package rollback

import grails.test.mixin.*
import grails.test.mixin.support.*
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Foo)
@TestMixin(GrailsUnitTestMixin)
class FooSpec extends Specification {

    void "Foo.value must be set if the instance is valid"() {
        expect:
        !new Foo(value: null).validate()
        new Foo(value: 'bar').validate()
    }
}
